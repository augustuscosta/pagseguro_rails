class ProdutosController < ApplicationController

  # EXEMPLO DE MÉTODO PARA SER UTILIZADO O PAGAMENTO PELO BROWSER
  def index
    produto = Produto.find(1)
    redirect_to produto.payment_request
  end


  # EXEMPLO DE MÉTODO PARA SER UTILIZADO O PAGAMENTO PELO MOBILE
  def show
    produto = Produto.find(params[:id])
    render json: {url: produto.payment_request},status:200
  end

end